import { UsergroupService } from './shared/services/usergroup.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from './shared/services/cookie.service';
import { ApiService } from './shared/services/api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './shared/guard/AuthGuard';
import { HttpClientModule } from '@angular/common/http';
import { DeploymentService } from './shared/services/deployment.service';
import { NamespaceService } from './shared/services/namespace.service';
import { ServiceService } from './shared/services/service.service';
import { ProjectService } from './shared/services/project.service';
import { PODService } from './shared/services/pod.service';
import { SecretService } from './shared/services/secret.service';
import { NodeService } from './shared/services/node.service';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    ApiService,
    AuthService,
    AuthGuard,
    CookieService,
    UsergroupService,
    DeploymentService,
    NamespaceService,
    ServiceService,
    ProjectService,
    PODService,
    SecretService,
    NodeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
