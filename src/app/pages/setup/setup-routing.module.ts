import { ServicesComponent } from './services/services.component';
import { UsergroupsComponent } from './usergroups/usergroups.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SetupComponent } from './setup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeploymentsComponent } from './deployments/deployments.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { CreateDeploymentComponent } from './create-deployment/create-deployment.component';
import { SecretsComponent } from './secrets/secrets.component';
import { CreateSecretComponent } from './create-secret/create-secret.component';
import { CreateServiceComponent } from './create-service/create-service.component';
import { NodesComponent } from './nodes/nodes.component';


const routes: Routes = [
  { path: '', component: SetupComponent, children: [
    { path: 'change-password', component: ChangePasswordComponent},
    { path: 'usergroups', component: UsergroupsComponent},
    { path: 'services', component: ServicesComponent},
    { path: 'secrets', component: SecretsComponent},
    { path: 'deployments', component: DeploymentsComponent},
    { path: 'nodes', component: NodesComponent},
    { path: 'create-project', component: CreateProjectComponent},
    { path: 'create-deployment', component: CreateDeploymentComponent},
    { path: 'create-secret', component: CreateSecretComponent},
    { path: 'create-service', component: CreateServiceComponent}
  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule { }
