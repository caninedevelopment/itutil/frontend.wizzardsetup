import { ChangePassword } from './../../../shared/model/change-password';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  success = false;
  error = false;

  passwordFG: FormGroup;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.passwordFG = this.formBuilder.group({
      oldPassword: new FormControl(null, [Validators.required]),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required)
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  ngOnInit(): void {
  }

  changePassword() {
    if (this.passwordFG.valid) {
      const cP = new ChangePassword();
      cP.newPassword = this.passwordFG.controls.password.value;
      cP.oldPassword = this.passwordFG.controls.oldPassword.value;
      this.authService.changePassword(cP).subscribe(
        (res) => {
          console.log(res);
          this.error = false;
          this.success = true;
        },
        error => {
          console.log(error);
          this.error = true;
          this.success = false;
        }
      );
    } else {
      this.passwordFG.markAllAsTouched();
    }
  }
}

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}
