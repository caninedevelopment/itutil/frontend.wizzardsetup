import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SetupRoutingModule } from './setup-routing.module';
import { NgModule } from '@angular/core';
import { SetupComponent } from './setup.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ServicesComponent } from './services/services.component';
import { UsergroupsComponent } from './usergroups/usergroups.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { DeploymentsComponent } from './deployments/deployments.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { CreateDeploymentComponent } from './create-deployment/create-deployment.component';
import { SecretsComponent } from './secrets/secrets.component';
import { CreateSecretComponent } from './create-secret/create-secret.component';
import { CreateServiceComponent } from './create-service/create-service.component';
import { NodesComponent } from './nodes/nodes.component';



@NgModule({
  declarations: [SetupComponent, ChangePasswordComponent, ServicesComponent, UsergroupsComponent, DeploymentsComponent, CreateProjectComponent, CreateDeploymentComponent, SecretsComponent, CreateSecretComponent, CreateServiceComponent, NodesComponent],
  imports: [
    CommonModule,
    SetupRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTreeModule,
    MatIconModule,
    MatSelectModule,
    ReactiveFormsModule
  ]
})
export class SetupModule { }
