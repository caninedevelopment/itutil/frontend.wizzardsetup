import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from 'src/app/shared/services/service.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['./create-service.component.scss']
})
export class CreateServiceComponent implements OnInit {

  namespace: string = null;

  serviceFG: FormGroup;

  get ports() : FormArray {
    return this.serviceFG.controls.ports as FormArray
  }

  constructor(
    private route: ActivatedRoute,
    private serviceService: ServiceService
  ) { 
    this.serviceFG = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      labelKey: new FormControl(null, [Validators.required]),
      labelValue: new FormControl(null, [Validators.required]),
      type: new FormControl(null, [Validators.required]),
      ports: new FormArray([])
    })

    this.addPort();
  }

  ngOnInit(): void {
    this.namespace = this.route.parent.snapshot.params.namespace;
  }

  addPort() {
    const fg = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      port: new FormControl(null, [Validators.required]),
      nodePort: new FormControl(null),
      protocol: new FormControl(null),
    });

    (this.serviceFG.controls.ports as FormArray).push(fg);
  }

  save() {
    if (this.serviceFG.valid) {
      const values = this.serviceFG.value;
      values.namespace = this.namespace;
      console.log(values);
      this.serviceService.create(values).subscribe(
        (res) => {
          console.log(res);
        }
      );
    } else{
      this.serviceFG.markAllAsTouched();
    }
  }
}
