import { Component, OnInit } from '@angular/core';
import { SecretService } from 'src/app/shared/services/secret.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-secrets',
  templateUrl: './secrets.component.html',
  styleUrls: ['./secrets.component.scss']
})
export class SecretsComponent implements OnInit {

  namespace: string = null;

  secrets: string[] = [];
  secret: string = null;
  
  constructor(
    private secretService: SecretService,
    private route: ActivatedRoute
  ) { 
    this.namespace = this.route.parent.snapshot.params.namespace;
  }

  ngOnInit(): void {
    this.secretService.getAll(this.namespace).subscribe(
      (res) => {
        this.secrets = res.secrets
        console.log(res);
        if (this.route.snapshot.queryParams.name !== undefined) {
          this.onEdit(this.route.snapshot.queryParams.name);
        }
      }
    );

    this.route.queryParams.subscribe(
      (res) => {
        this.onEdit(res.name);
      }
    );
  }

  onEdit(name: string) {
    if (this.secrets.length < 1) {
      return;
    }
    this.secret = name;
  }

  deleteSecret() {
    if (this.secret === null) {
      return;
    }
    this.secretService.delete(this.namespace, this.secret).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }
}
