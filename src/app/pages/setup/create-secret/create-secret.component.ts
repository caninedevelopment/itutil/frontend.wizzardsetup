import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SecretService } from 'src/app/shared/services/secret.service';
import { Secret } from 'src/app/shared/model/secret';

@Component({
  selector: 'app-create-secret',
  templateUrl: './create-secret.component.html',
  styleUrls: ['./create-secret.component.scss']
})
export class CreateSecretComponent implements OnInit {

  namespace: string = null;

  secretFG: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private secretService: SecretService
  ) { 
    this.secretFG = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      server: new FormControl(null, [Validators.required]),
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit(): void {
    this.namespace = this.route.parent.snapshot.params.namespace;
  }

  save() {
    if (this.secretFG.valid) {
      const s = new Secret(
        this.namespace,
        this.secretFG.controls.name.value,
        this.secretFG.controls.server.value,
        this.secretFG.controls.username.value,
        this.secretFG.controls.password.value,
        this.secretFG.controls.email.value
      );

      this.secretService.create(s).subscribe(
        (res) => {
          console.log(res);
        }
      );
    } else {
      this.secretFG.markAllAsTouched();
    }
  }
}
