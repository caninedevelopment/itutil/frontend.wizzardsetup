import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/shared/services/service.service';
import { Service } from 'src/app/shared/model/service';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  namespace = null;

  services: Service[] = [];
  service: Service = null;

  serviceFG: FormGroup = null;

  constructor(
    private serviceService: ServiceService,
    private route: ActivatedRoute
  ) { 
    this.serviceFG = new FormGroup({
      name: new FormControl(null),
      nodeType: new FormControl(null),
      ports: new FormGroup({
        name: new FormControl(null),
        port: new FormControl(null),
        nodePort: new FormControl(null),
        protocol: new FormControl(null)
      })
    });
  }

  ngOnInit(): void {
    this.namespace = this.route.parent.snapshot.params.namespace;

    this.serviceService.getServices(this.namespace).subscribe(
      (res) => {
        this.services = res.services;
        if (this.route.snapshot.queryParams.name !== undefined) {
          this.onEdit(this.route.snapshot.queryParams.name);
        }
        console.log(res);
      }
    );

    this.route.queryParams.subscribe(
      (res) => {
        this.onEdit(res.name);
      }
    );
  }

  onEdit(name) {
    const s = this.services.find(x => x.name === name);
    if (s !== undefined) {
      this.service = s;
      this.serviceFG.patchValue(s);
      if (s.ports.length === 1) {
        (this.serviceFG.controls.ports as FormGroup).patchValue(s.ports[0]);
      }
    }
  }

  deleteService() {
    this.serviceService.delete(this.namespace, this.service.name).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }
}
