import { LoginResult } from './../../../shared/model/login-result';
import { UserGroup } from './../../../shared/model/usergroup';
import { Claim } from './../../../shared/model/claim';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UsergroupService } from './../../../shared/services/usergroup.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Microservice, RequiredClaim } from 'src/app/shared/model/Microservice';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { AuthService } from 'src/app/shared/services/auth.service';
import { guid } from 'src/app/shared/functions/guid';

@Component({
  selector: 'app-usergroups',
  templateUrl: './usergroups.component.html',
  styleUrls: ['./usergroups.component.scss']
})
export class UsergroupsComponent implements OnInit {

  saved = false;
  error = false;

  usergroups = [];

  usergroup: UserGroup = null;

  claims: RequiredClaim[] = [];

  claimObj: object = {};

  ugFG: FormGroup = null;

  // Mat tree
  treeControl: FlatTreeControl<ItemFlatNode>;
  dataSource: MatTreeFlatDataSource<ItemNode, ItemFlatNode>;
  checklistSelection = new SelectionModel<ItemFlatNode>(true /* multiple */);
  nestedNodeMap = new Map<ItemNode, ItemFlatNode>();
  flatNodeMap = new Map<ItemFlatNode, ItemNode>();
  treeFlattener: MatTreeFlattener<ItemNode, ItemFlatNode>;
  getLevel = (node: ItemFlatNode) => node.level;
  hasChild = (_: number, nodeData: ItemFlatNode) => nodeData.expandable;
  deselectAll = (nodeData: ItemFlatNode) => this.checklistSelection.deselect(nodeData);
  isExpandable = (node: ItemFlatNode) => node.expandable;
  getChildren = (node: ItemNode): ItemNode[] => node.children;
  transformer = (node: ItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.item
      ? existingNode
      : new ItemFlatNode();
    flatNode.item = node.item;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  constructor(
    private usergroupService: UsergroupService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<ItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  ngOnInit(): void {
    this.usergroupService.getUsergroups().toPromise().then(
      (res) => {
        console.log(res);
        this.usergroups = res.usergroups;
        this.usergroupService.getMicroservices().toPromise().then(
          (mss) => {
            console.log(mss);
            this.claims = [];

            const svs = mss.services as Microservice[];
            for (const ms of svs) {
              for (const opr of ms.operationDescriptions) {
                if (opr.requiredClaims !== null) {
                  this.claims = this.claims.concat(opr.requiredClaims);
                }
              }
            }
            this.claimStrToClaimTreeObj();
            const id = this.route.snapshot.queryParams.id;
            if (id !== undefined) {
              this.editUG(id);
            }
            console.log(this.claims);
          }
        );
      }
    );

    this.route.queryParams.subscribe(
      (param) => {
        this.editUG(param.id);
      }
    );
  }

  createFG() {
    this.ugFG = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      claims: new FormArray([])
    });
  }

  editUG(id: string) {
    this.error = false;
    this.saved = false;
    const ug = this.usergroups.find(x => x.usergroupId === id);
    if (ug !== undefined) {
      if (this.ugFG === null) {
        this.createFG();
      }
      this.ugFG.patchValue(ug);
      this.usergroup = ug;
      this.selectItems();
    }
  }

  save() {
    console.log('save');
    console.log(this.getSelectedClaims());
    if (this.ugFG.valid) {
      this.usergroup.claims = this.getSelectedClaims();
      this.usergroup.name = this.ugFG.controls.name.value;

      if (this.usergroup.usergroupId === 'NewGroup') {
        this.usergroup.usergroupId = guid();
        this.usergroupService.addUserGroup(this.usergroup).toPromise().then(
          (res) => {
            console.log(res);
            this.error = false;
            this.saved = true;

            this.router.navigate(['/usergroups'], { queryParams: { id: this.usergroup.usergroupId } });

            this.authService.verify().toPromise().then(
              (vRes: LoginResult) => {
                console.log(vRes);
                this.authService.setToken(vRes.tokenId, new Date(vRes.validUntil));
              }
            );
          },
          error => {
            this.error = true;
            this.saved = false;
          }
        )
      } else {
        this.usergroupService.updateUserGroup(this.usergroup).toPromise().then(
          (res) => {
            console.log(res);
            this.error = false;
            this.saved = true;

            this.authService.verify().toPromise().then(
              (vRes: LoginResult) => {
                console.log(vRes);
                this.authService.setToken(vRes.tokenId, new Date(vRes.validUntil));
              }
            );
          },
          error => {
            this.error = true;
            this.saved = false;
          }
        );
      }
    } else {
      this.ugFG.markAllAsTouched();
    }
  }

  delete() {
    if (this.usergroup.usergroupId === '00000000-0000-0000-0000-000000000000') {
      return;
    }
    this.usergroupService.deleteUsergroup(this.usergroup.usergroupId).toPromise().then(
      (res) => {
        console.log(res);
        this.error = false;
        this.saved = true;

        const index = this.usergroups.indexOf(x => x.usergroupId === this.usergroup.usergroupId);
        this.usergroups = this.usergroups.splice(index, 1);

        this.router.navigate(['/usergroups']);

        this.authService.verify().toPromise().then(
          (vRes: LoginResult) => {
            console.log(vRes);
            this.authService.setToken(vRes.tokenId, new Date(vRes.validUntil));
          }
        );
      },
      error => {
        this.error = true;
        this.saved = false;
      }
    )
  }

  addUsergroup() {

    const oNew = this.usergroups.find(x => x.usergroupId === 'NewGroup');
    if (oNew === undefined) {

      console.log('creating ug');
      const ug = new UserGroup();
      ug.usergroupId = 'NewGroup';
      ug.claims = [];
      ug.name = 'New group';
      ug.userIds = [];
      this.usergroups.push(ug);
    }

    this.router.navigate(['/usergroups'], { queryParams: { id: 'NewGroup' } });
    this.editUG('NewGroup');
  }


  // Mat tree

  getSelectedClaims(): Claim[] {
    const claims: Claim[] = [];
    for (const selected of this.checklistSelection.selected) {
      if (selected.expandable === false) {
        let path = selected.item;
        let node = selected;
        while (node !== null) {
          if (path !== node.item) {
            path = node.item + '.' + path;
          }

          node = this.getParentNode(node);
        }
        if (selected.level + 1 === path.split('.').length) { // For at tjekke om korrekt path. (Da der er for mange ellers somhow)
          const newClaim = new Claim();
          newClaim.key = path;
          newClaim.value = 'true';
          claims.push(newClaim);
        }
      }
    }
    return claims;
  }

  /** Convert alle claim str to obj, to use used in tree */
  claimStrToClaimTreeObj() {
    const obj = {};

    for (const claimStr of this.claims) {
      const parts = claimStr.key.split('.');

      let lastObj = obj;
      for (let i = 0; i < parts.length; i++) {
        if (lastObj.hasOwnProperty(parts[i]) === false) {
          if (i === parts.length - 1) {
            lastObj[parts[i]] = null; // Set to null, since there are no children
          } else {
            lastObj[parts[i]] = {}; // Set to object, because it's a "group"
          }
        }
        lastObj = lastObj[parts[i]];
      }
    }
    this.claimObj = obj;
    const buildedObj = this.buildFileTree(this.claimObj, 0);
    this.dataSource.data = buildedObj;
    this.selectItems();
  }

  buildFileTree(obj: { [key: string]: any }, level: number): ItemNode[] {
    return Object.keys(obj).reduce<ItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new ItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Select all items from group */
  selectItems() {
    for (const selected of this.checklistSelection.selected) {
      this.deselectAll(selected);
    }
    if (this.usergroup !== null) {
      for (const claim of this.usergroup.claims) {
        this.selectItem(claim.key);
      }
    }
  }

  /** Select item via string */
  selectItem(key: string) {
    const selectedParts = key.split('.');
    let children = this.treeControl.dataNodes;
    console.log(this.treeControl);
    console.log(children);
    for (let i = 0; i < selectedParts.length; i++) {
      for (const child of children) {
        if (child.item === selectedParts[i]) {
          if (i === selectedParts.length - 1) {
            this.todoItemSelectionToggle(child);
          } else {
            children = this.treeControl.getDescendants(child);
            break;
          }
        }
      }
    }
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: ItemFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: ItemFlatNode): void {
    let parent: ItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: ItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: ItemFlatNode): ItemFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: ItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: ItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: ItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.checkAllParentsSelection(node);
  }
}

// Mat tree
class ItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

class ItemNode {
  children: ItemNode[];
  item: string;
}

