import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ProjectService } from 'src/app/shared/services/project.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {

  namespaces = ['default'];
  namespace = 'default';

  constructor(
    private authService: AuthService,
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.route.params.subscribe(
      (res) => {
        this.namespace = res.namespace;
        console.log(res);
      }
    );


    this.projectService.getProjects().subscribe(
      (res) => {
        for (let p of res.projects) {
          const op = this.namespaces.find(x => x === p.name);
          if (op === undefined) {
            this.namespaces.push(p.name);
          }
        }
        console.log(res);
      }
    );
  }

  changeNamespace(event) {
    this.router.navigate(['/project', event.value]);
    console.log(event.value);
  }

  logout() {
    this.authService.logout();
  }
}
