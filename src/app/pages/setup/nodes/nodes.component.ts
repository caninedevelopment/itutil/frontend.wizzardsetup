import { Component, OnInit } from '@angular/core';
import { NodeService } from 'src/app/shared/services/node.service';

@Component({
  selector: 'app-nodes',
  templateUrl: './nodes.component.html',
  styleUrls: ['./nodes.component.scss']
})
export class NodesComponent implements OnInit {

  nodes: any[] = [];

  constructor(
    private nodeService: NodeService
  ) { }

  ngOnInit(): void {
    this.nodeService.getAll().subscribe(
      (res) => {
        console.log(res);
        this.nodes = res.nodes;
      }
    );
  }

}
