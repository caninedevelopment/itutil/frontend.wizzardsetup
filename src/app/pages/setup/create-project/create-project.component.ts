import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ProjectService } from 'src/app/shared/services/project.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {

  projectFG: FormGroup;

  constructor(
    private projectService: ProjectService
  ) { 
    this.projectFG = new FormGroup({
      name: new FormControl(null)
    })
  }

  ngOnInit(): void {
  }

  create() {
    if (this.projectFG.valid) {
      this.projectService.createProject(this.projectFG.controls.name.value).subscribe(
        (res) => {
          console.log(res);
          location.reload();
        }
      );
    } else {
      this.projectFG.markAllAsTouched();
    }
  }
}
