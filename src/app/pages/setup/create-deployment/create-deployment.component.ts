import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DeploymentService } from 'src/app/shared/services/deployment.service';
import { Deployment } from 'src/app/shared/model/deployment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-deployment',
  templateUrl: './create-deployment.component.html',
  styleUrls: ['./create-deployment.component.scss']
})
export class CreateDeploymentComponent implements OnInit {

  namespace = null;

  deploymentFG: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private deploymentService: DeploymentService
  ) { 
    this.namespace = this.route.parent.snapshot.params.namespace;

    this.deploymentFG = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      containerName: new FormControl(null, [Validators.required]),
      containerImage: new FormControl(null, [Validators.required]),
      labelKey: new FormControl(null, [Validators.required]),
      labelValue: new FormControl(null, [Validators.required]),
      imagePullSecret: new FormControl(null)
    })
  }

  ngOnInit(): void {
  }

  save() {
    if (this.deploymentFG.valid) {
      const d = new Deployment();
      d.name = this.deploymentFG.controls.name.value;
      d.containerName = this.deploymentFG.controls.containerName.value;
      d.containerImage = this.deploymentFG.controls.containerImage.value;
      d.namespace = this.namespace;
      d.labels = [{key: this.deploymentFG.controls.labelKey.value, value: this.deploymentFG.controls.labelValue.value}];
      if (this.deploymentFG.controls.imagePullSecret.value !== null && this.deploymentFG.controls.imagePullSecret.value.trim() !== '') {
        d.imagePullSecrets = [this.deploymentFG.controls.imagePullSecret.value];
      }

      this.deploymentService.create(d).subscribe(
        (res) => {
          console.log(res);
        }
      );
    } else {
      this.deploymentFG.markAllAsTouched();
    }
  }
}
