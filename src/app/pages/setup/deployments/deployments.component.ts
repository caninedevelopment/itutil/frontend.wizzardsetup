import { Component, OnInit } from '@angular/core';
import { DeploymentService } from 'src/app/shared/services/deployment.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Deployment } from 'src/app/shared/model/deployment';
import { ActivatedRoute } from '@angular/router';
import { PODService } from 'src/app/shared/services/pod.service';

@Component({
  selector: 'app-deployments',
  templateUrl: './deployments.component.html',
  styleUrls: ['./deployments.component.scss']
})
export class DeploymentsComponent implements OnInit {

  namespace = null;

  deployments: Deployment[];
  deployment: Deployment = null;

  deploymentFG: FormGroup = null;

  pods: [] = [];

  constructor(
    private deploymentService: DeploymentService,
    private podService: PODService,
    private route: ActivatedRoute
  ) {
    this.deploymentFG = new FormGroup({
      name: new FormControl(null),
      namespace: new FormControl(null),
      containerName: new FormControl(null),
      containerImage: new FormControl(null),
    });
  }

  ngOnInit() {
    this.namespace = this.route.parent.snapshot.params.namespace;

    this.getAll();

    this.route.queryParams.subscribe(
      (res) => {
        this.onEdit(res.name);
      }
    );
  }

  deleteDeployment() {
    if (this.deployment === null) {
      return;
    }
    this.deploymentService.delete(this.namespace, this.deployment.name).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }

  getAll() {
    this.deploymentService.getAll(this.namespace).subscribe(
      (res) => {
        this.deployments = res.deployments;
        console.log(res);
        if (this.route.snapshot.queryParams.name !== undefined) {
          this.onEdit(this.route.snapshot.queryParams.name);
        }
      }
    );
  }

  onEdit(name) {
    if (name === undefined || this.deployments === undefined) {
      return;
    }
    this.pods = [];
    console.log(name);
    const d = this.deployments.find(x => x.name === name);

    this.podService.getPods(this.namespace, name).subscribe(
      (res) => {
        this.pods = res.pods;
        console.log(res);
      }
    );

    console.log(d);
    if (d !== undefined) {
      this.deployment = d;
      this.deploymentFG.patchValue(d);
    }
  }
}