import { Router } from '@angular/router';
import { LoginResult } from '../../shared/model/login-result';
import { Login } from '../../shared/model/login';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usrOrPassInvalid = false;

  loginFG: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/']);
      return;
    }

    const login = new Login();
    login.username = 'root';
    login.password = 'defaultPWD';
    this.authService.login(login).subscribe(
      (res: LoginResult) => {
        this.authService.setToken(res.tokenId, new Date(res.validUntil));
        this.router.navigate(['/wizzard']);
      },
      error => {
        this.usrOrPassInvalid = true;
        console.log(error);
      }
    );

    this.loginFG = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    });
  }

  ngOnInit(): void {
  }

  login() {
    if (this.loginFG.valid) {
      this.usrOrPassInvalid = false;
      const login = new Login();
      login.username = this.loginFG.controls.username.value;
      login.password = this.loginFG.controls.password.value;
      this.authService.login(login).subscribe(
        (res: LoginResult) => {
          this.authService.setToken(res.tokenId, new Date(res.validUntil));
          this.router.navigate(['/']);
        },
        error => {
          this.usrOrPassInvalid = true;
          console.log(error);
        }
      );
    } else {
      this.loginFG.markAllAsTouched();
    }
  }
}
