import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { WizzardRoutingModule } from './wizzard-routing.module';
import { NgModule } from '@angular/core';
import { WizzardComponent } from './wizzard.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [WizzardComponent, ChangePasswordComponent],
  imports: [
    WizzardRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule
  ]
})
export class WizzardModule { }
