export class Deployment {
    public name: string;
    public namespace: string;
    public containerName: string;
    public containerImage: string;
    public imagePullSecrets: string[];
    public labels: any[];
}