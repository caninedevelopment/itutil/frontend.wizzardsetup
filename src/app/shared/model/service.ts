import { ServicePort } from './service-port';

export class Service {
    public name: string;
    public noteType: string;
    public ports: ServicePort[];
}