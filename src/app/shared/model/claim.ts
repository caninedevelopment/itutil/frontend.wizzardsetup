export class Claim {
    public key: string;
    public value: string;
    public scope: number;
}
