import { Claim } from './claim';

export class LoginResult {
    public claims: Claim[];
    public tokenId: string;
    public userId: string;
    public validUntil: Date;
}
