export class Secret {
    constructor(
        public namespace: string,
        public name: string,
        public server: string,
        public username: string,
        public password: string,
        public email: string
    ) {}
}