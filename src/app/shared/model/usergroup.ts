import { Claim } from './claim';

export class UserGroup {
    usergroupId: string;
    name: string;
    userIds: [];
    claims: Claim[];
}
