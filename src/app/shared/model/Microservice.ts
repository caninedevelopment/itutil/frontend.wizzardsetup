export declare class Microservice {
    operationDescriptions: MicroserviceOperation[];
    serviceName: string;
    version: MicroserviceVersion;
}
export declare class MicroserviceOperation {
    dataInputExample: any;
    dataOutputExample: any;
    description: string;
    operation: string;
    requiredClaims: RequiredClaim[];
    type: OperationType;
}
export declare class RequiredClaim {
    key: string;
    description: any;
    dataContext: string;
}
export declare enum OperationType {
    Event = 0,
    Get = 1,
    Update = 2,
    Insert = 3,
    Delete = 4
}

export declare class MicroserviceVersion {
    version: string;
    build: number;
    major: number;
    minor: number;
    revision: number;
}
