export class ServicePort {
    public name: string;
    public port: number;
    public nodePort: number;
    public protocol: string;
}