import { ChangePassword } from './../model/change-password';
import { CookieService } from './cookie.service';
import { ApiService } from './api.service';
import { Login } from './../model/login';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {

    tokenStr = 'token_id';
    tokenDateStr = 'token_date';

    constructor(
        private router: Router,
        private apiService: ApiService,
        private cookieService: CookieService
    ) {
    }

    /** Call server for login */
    login(loginInfo: Login): Observable<any> {
        return this.apiService.post('token/v2/login', loginInfo);
    }

    /** Loging out */
    logout(): void {
        this.setToken(null);
        this.router.navigate(['/login']);
    }

    /** Return true if is logged in */
    isLoggedIn() {
        if (this.apiService.getToken() === null) {
            this.setToken(this.cookieService.get(this.tokenStr), new Date(this.cookieService.get(this.tokenDateStr)));
        }
        return this.apiService.getToken() !== null;
    }

    /** Set token if succeded login */
    setToken(token: string, date: Date = null) {
        if (token === null) {
            this.cookieService.delete(this.tokenStr);
            this.cookieService.delete(this.tokenDateStr);
            this.apiService.setToken(null);
            return;
        }
        this.cookieService.set(this.tokenStr, token, date);
        this.cookieService.set(this.tokenDateStr, date.toUTCString(), date);
        this.apiService.setToken(token);
    }

    /** change password */
    changePassword(cP: ChangePassword): Observable<any> {
        return this.apiService.post('user/v2/changePassword', cP);
    }

    /** Verify token */
    verify() {
        return this.apiService.post('token/v2/verify', { tokenId: this.apiService.getToken() });
    }
}
