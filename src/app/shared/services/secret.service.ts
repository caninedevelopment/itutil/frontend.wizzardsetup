import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Secret } from '../model/secret';

@Injectable()
export class SecretService {
    constructor(
        private apiService: ApiService
    ) {}

    getAll(namespace: string) {
        return this.apiService.get('kubernetessecrets/v1/GetAll?namespace=' + namespace);
    }

    delete(namespace: string, name: string) {
        return this.apiService.post('kubernetessecrets/v1/Delete', {namespace, name});
    }

    create(secret: Secret) {
        return this.apiService.post('kubernetessecrets/v1/Create', secret);
    }
}