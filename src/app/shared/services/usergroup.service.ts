import { UserGroup } from './../model/usergroup';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable()
export class UsergroupService {
    constructor(
        private apiService: ApiService
    ) {

    }

    /** Get all usergroups from server */
    getUsergroups() {
        return this.apiService.get('usergroup/v2/getall');
    }

    /** Update usergroup on server */
    updateUserGroup(usergroup: UserGroup) {
        return this.apiService.post('usergroup/v2/update', usergroup);
    }

    /** Add usergroup on server */
    addUserGroup(usergroup: UserGroup) {
        return this.apiService.post('usergroup/v2/create', usergroup);
    }

    /** Delete usergroup on server */
    deleteUsergroup(id: string) {
        return this.apiService.delete('usergroup/v2/delete?usergroupId=' + id);
    }

    /** Get microservices from server */
    getMicroservices() {
        return this.apiService.get('microservicediscovery.v1.get');
    }
}
