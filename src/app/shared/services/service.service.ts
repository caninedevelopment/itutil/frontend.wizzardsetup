import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class ServiceService {
    constructor(
        private apiService: ApiService
    ) {}

    getServices(namespace: string) {
        return this.apiService.get('kubernetesservices/v1/GetAll?namespace=' + namespace);
    }
    
    create(service: any) {
        return this.apiService.post('kubernetesservices/v1/Create', service);
    }

    delete(namespace: string, name: string) {
        return this.apiService.post('kubernetesservices/v1/Delete', {namespace, name});
    }
}