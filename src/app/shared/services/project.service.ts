import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class ProjectService {
    constructor(
        private apiService: ApiService
    ) {}

    getProjects() {
        return this.apiService.get('kubernetesprojects/v1/GetAll');
    }

    createProject(name: string) {
        return this.apiService.post('kubernetesprojects/v1/Create', {name});
    }
}