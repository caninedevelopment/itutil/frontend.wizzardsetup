import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Deployment } from '../model/deployment';

@Injectable()
export class DeploymentService {

    constructor(
        private apiService: ApiService
    ) {}

    getAll(namespace: string) {
        return this.apiService.get('kubernetesdeployments/v1/GetAll?namespace=' + namespace);
    }

    get(namespace: string, name: string, image: string) {
        return this.apiService.get('kubernetesdeployments/v1/Get?namespace=' + namespace + '&name=' + name + '&image=' + image);
    }

    create(deployment: Deployment) {
        return this.apiService.post('kubernetesdeployments/v1/Create', deployment);
    }

    delete(namespace: string, name: string) {
        return this.apiService.post('kubernetesdeployments/v1/Delete', {namespace, name});
    }
}