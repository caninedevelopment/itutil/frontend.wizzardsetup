import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {

    token = null;
    baseUrl = 'http://192.168.44.35:31000/';

    constructor(
        private http: HttpClient
    ) {
    }

    /** Set token, for api to use */
    setToken(token: string): void {
        this.token = token;
    }

    /** Get current token */
    getToken(): string {
        return this.token;
    }

    /** Get from server */
    get(url: string): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        if (this.token !== null) {
            headers = headers.append('TokenId', this.token);
        }

        return this.http.get(this.baseUrl + url, {
            headers
        });
    }

    /** Post to server */
    post(url: string, data: any): Observable<any> {

        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        if (this.token !== null) {
            headers = headers.append('TokenId', this.token);
        }

        const model = JSON.stringify(data);

        return this.http.post(this.baseUrl + url, model, {
            headers
        });
    }

    /** Put to server */
    put(url: string, data: any): Observable<any> {

        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        if (this.token !== null) {
            headers = headers.append('TokenId', this.token);
        }

        const model = JSON.stringify(data);

        return this.http.put(this.baseUrl + url, model, {
            headers
        });
    }

    /** Delete from server */
    delete(url: string): Observable<any> {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        if (this.token !== null) {
            headers = headers.append('TokenId', this.token);
        }

        return this.http.delete(this.baseUrl + url, {
            headers
        });
    }
}
