import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class NodeService {
    constructor(
        private apiService: ApiService
    ) {}

    getAll() {
        return this.apiService.get('kubernetesnodes/v1/GetAll');
    }
}