import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CookieService {

    /** Set cookie */
    set(key: string, value: string, date: Date): void {
        if (key !== undefined && key !== null && value !== undefined && value !== null) {
            if (date !== undefined && date !== null) {
                document.cookie = key + '=' + value + '; expires=' + date.toUTCString() + '; path=/';
            }
        }
    }

    /** Get cookie */
    get(key: string): any {
        const parts = document.cookie.split(';');
        const part = parts.find(x => x.trim().startsWith(key + '='));
        if (part !== undefined) {
            const value = part.substr(part.indexOf('=') + 1);
            return value;
        }
        return null;
    }

    /** Remove cookie */
    delete(key: string): void {
        document.cookie = key + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';
    }
}
