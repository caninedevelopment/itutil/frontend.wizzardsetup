import { Injectable } from "@angular/core";
import { ApiService } from './api.service';

@Injectable()
export class PODService {
    constructor(
        private apiService: ApiService
    ) {}

    getPods(namespace: string, deploymentName: string) {
        return this.apiService.get('kubernetespods/v1/GetAll?namespace=' + namespace + '&deploymentName=' + deploymentName);
    }
}