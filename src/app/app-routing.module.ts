import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/AuthGuard';


const routes: Routes = [
  { path: 'project/:namespace', loadChildren: () => import('./pages/setup/setup.module').then(m => m.SetupModule), canActivate: [AuthGuard] },
  { path: 'wizzard', loadChildren: () => import('./pages/wizzard/wizzard.module').then(m => m.WizzardModule), canActivate: [AuthGuard] },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: '**', redirectTo: '/project/default', canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
